* [Getting Started](##Getting%20Started)
* [ERD Diagram](##ERD%20Diagram)

## Getting Started

 1. Download the tuition program and put to any directory, for eg: ``C:\Users\user\Documents\``
 2. Create a Database named **tuition** and make sure the database connection setting is same as in file:
    ```
    tuition > src > app-config.ts
    ```
 3. Run the sql to create tables at ``tuition > sql > creates tables.sql``   
 4. To install necessary package, run ``npm install`` in command prompt (**under tuition program directory**):
 5. To start the program, run in command prompt:
    ```
    npm start
    ```
 6. If you want to stop it, in command prompt press ``CTRL``+``C``
 7. Now you able to test the API with program such as Postman.

## ERD Diagram
![ERD Diagram](images/ERD-diagram.png)