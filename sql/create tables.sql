/*
SQLyog Community v12.2.4 (64 bit)
MySQL - 5.7.24 : Database - tuition
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`tuition` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `tuition`;

/*Table structure for table `status` */

DROP TABLE IF EXISTS `status`;

CREATE TABLE `status` (
  `statusId` INT(11) NOT NULL AUTO_INCREMENT,
  `statusName` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`statusId`)
) ENGINE=INNODB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `status` */

INSERT  INTO `status`(`statusId`,`statusName`) VALUES 
(1,'Active'),
(2,'Suspend');

/*Table structure for table `student` */

DROP TABLE IF EXISTS `student`;

CREATE TABLE `student` (
  `studentId` INT(11) NOT NULL AUTO_INCREMENT,
  `firstName` VARCHAR(100) NOT NULL,
  `lastName` VARCHAR(100) NOT NULL,
  `email` VARCHAR(100) NOT NULL,
  `statusId` INT(11) DEFAULT NULL,
  PRIMARY KEY (`studentId`),
  UNIQUE KEY `IDX_a56c051c91dbe1068ad683f536` (`email`),
  KEY `FK_2a732e0a8abf2d3b2c3e97f2923` (`statusId`),
  CONSTRAINT `FK_2a732e0a8abf2d3b2c3e97f2923` FOREIGN KEY (`statusId`) REFERENCES `status` (`statusId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=INNODB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Table structure for table `teacher` */

DROP TABLE IF EXISTS `teacher`;

CREATE TABLE `teacher` (
  `teacherId` INT(11) NOT NULL AUTO_INCREMENT,
  `firstName` VARCHAR(100) NOT NULL,
  `lastName` VARCHAR(100) NOT NULL,
  `email` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`teacherId`),
  UNIQUE KEY `IDX_00634394dce7677d531749ed8e` (`email`)
) ENGINE=INNODB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;


/*Table structure for table `tuition` */

DROP TABLE IF EXISTS `tuition`;

CREATE TABLE `tuition` (
  `Id` SMALLINT(6) NOT NULL AUTO_INCREMENT,
  `teacherId` INT(11) DEFAULT NULL,
  `studentId` INT(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `IDX_121530f9889b0a59445080460a` (`teacherId`,`studentId`),
  KEY `FK_847dd834904a2fdc4be891c579d` (`studentId`),
  CONSTRAINT `FK_718de6b84f5734955d42ca85826` FOREIGN KEY (`teacherId`) REFERENCES `teacher` (`teacherId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_847dd834904a2fdc4be891c579d` FOREIGN KEY (`studentId`) REFERENCES `student` (`studentId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=INNODB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
