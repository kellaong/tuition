export class BaseResponse {
  isSuccess: boolean;
  response: any;
  message: string;
}