"use strict";
/*
This function is to change array of object to Array
Eg 1:
change
[{"teacherId":"3"},{"teacherId":"2"}]
to
{"teacherId":["3","2"]}

Eg 2:
change
[{"studEmail":"commonstudent1@gmail.com"},{"studEmail":"commonstudent2@gmail.com"}]
to
{"students":["commonstudent1@gmail.com","commonstudent2@gmail.com"]

*/
Object.defineProperty(exports, "__esModule", { value: true });
function FuncArrayObjectToArray(arr, ArrFieldName) {
    let resultArr = [];
    for (var index in arr) {
        // console.log(teacherRes[index].teacherId);
        eval("resultArr.push(arr[index]." + ArrFieldName + ");");
    }
    return resultArr;
}
exports.FuncArrayObjectToArray = FuncArrayObjectToArray;
/*This function is to validate email */
function FuncValidateEmail(mail) {
    if (mail.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi) != null) {
        return true;
    }
    else {
        return false;
    }
}
exports.FuncValidateEmail = FuncValidateEmail;
//# sourceMappingURL=functions.js.map